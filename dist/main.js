(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shopping_bag_view_bag_view_bag_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shopping-bag/view-bag/view-bag.component */ "./src/app/shopping-bag/view-bag/view-bag.component.ts");




var routes = [
    {
        path: '',
        component: _shopping_bag_view_bag_view_bag_component__WEBPACK_IMPORTED_MODULE_3__["ViewBagComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\r\n    margin-top: 20px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'assignment-app';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _shopping_bag_shopping_bag_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shopping-bag/shopping-bag.module */ "./src/app/shopping-bag/shopping-bag.module.ts");







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _shopping_bag_shopping_bag_module__WEBPACK_IMPORTED_MODULE_6__["ShoppingBagModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/shopping-bag/shopping-bag.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/shopping-bag/shopping-bag.module.ts ***!
  \*****************************************************/
/*! exports provided: ShoppingBagModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingBagModule", function() { return ShoppingBagModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _view_bag_view_bag_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view-bag/view-bag.component */ "./src/app/shopping-bag/view-bag/view-bag.component.ts");
/* harmony import */ var _shopping_bag_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shopping-bag.service */ "./src/app/shopping-bag/shopping-bag.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");







var ShoppingBagModule = /** @class */ (function () {
    function ShoppingBagModule() {
    }
    ShoppingBagModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_view_bag_view_bag_component__WEBPACK_IMPORTED_MODULE_3__["ViewBagComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"]
            ],
            providers: [_shopping_bag_service__WEBPACK_IMPORTED_MODULE_4__["ShoppingBagService"]],
        })
    ], ShoppingBagModule);
    return ShoppingBagModule;
}());



/***/ }),

/***/ "./src/app/shopping-bag/shopping-bag.service.ts":
/*!******************************************************!*\
  !*** ./src/app/shopping-bag/shopping-bag.service.ts ***!
  \******************************************************/
/*! exports provided: ShoppingBagService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingBagService", function() { return ShoppingBagService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var ShoppingBagService = /** @class */ (function () {
    function ShoppingBagService(http) {
        this.http = http;
        this.uri = 'http://localhost:3000';
    }
    ShoppingBagService.prototype.getOffers = function () {
        return this.http.get(this.uri + "/getOffers");
    };
    ShoppingBagService.prototype.getProducts = function () {
        return this.http.get(this.uri + "/getProducts");
    };
    ShoppingBagService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ShoppingBagService);
    return ShoppingBagService;
}());



/***/ }),

/***/ "./src/app/shopping-bag/view-bag/view-bag.component.css":
/*!**************************************************************!*\
  !*** ./src/app/shopping-bag/view-bag/view-bag.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".heading {\r\n  text-align: left;\r\n}\r\n.row{\r\n  margin: 25px;\r\n}\r\n.price{\r\n  text-align: right;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvcHBpbmctYmFnL3ZpZXctYmFnL3ZpZXctYmFnLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLFlBQVk7QUFDZDtBQUNBO0VBQ0UsaUJBQWlCO0FBQ25CIiwiZmlsZSI6InNyYy9hcHAvc2hvcHBpbmctYmFnL3ZpZXctYmFnL3ZpZXctYmFnLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGluZyB7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG4ucm93e1xyXG4gIG1hcmdpbjogMjVweDtcclxufVxyXG4ucHJpY2V7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/shopping-bag/view-bag/view-bag.component.html":
/*!***************************************************************!*\
  !*** ./src/app/shopping-bag/view-bag/view-bag.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-header\">\n  <h1>YOUR SHOPPING BAG</h1>\n  <hr style=\"height:10px; border:none; color:rgb(220, 223, 230); background-color:rgb(220, 223, 230);\">\n</div>\n<div class=\"form-flex\">\n  <table class=\"table\" style=\"width: 100%;\">\n    <thead>\n      <tr class=\"heading\">\n        <th>ITEMS</th>\n        <th style=\"width:15%\">NAME</th>\n        <th style=\"width:15%\">GENDER</th>\n        <th style=\"width:15%\">QTY</th>\n        <th style=\"width:15%\">PRICE</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let Product of ProductList\">\n        <td>\n          <div class=\"form-group\">\n            <img src=\"{{Product.image}}\" alt=\"\" />\n          </div>\n        </td>\n        <td>\n          <div class=\"form-group\">\n            {{Product.product_name}}\n          </div>\n        </td>\n        <td>\n          <div class=\"form-group\">\n            {{Product.gender}}\n          </div>\n        </td>\n        <td>\n          <div class=\"form-group\">\n            <input [type]=\"Product.type\" [name]=\"Product.name\" (change)=calculatePrice(ProductList,Product.value) class=\"form-control\" [(ngModel)]=\"Product.value\">\n          </div>\n        </td>\n        <td>\n          <div class=\"form-group\">\n            $ {{Product.product_price}}\n          </div>\n        </td>\n      </tr>\n\n\n    </tbody>\n  </table>\n  <hr style=\"height:10px; border:none; color:rgb(220, 223, 230); background-color:rgb(220, 223, 230);\"><br>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-3\">\n    <h6>Need Help or Any Questions</h6><br>\n    <p>Call At 9999-9900-0000</p>\n    <a href=''><u>Chat with one of our stylist</u></a><br>\n    <a href=''><u>See return & Exchange Policy</u></a>\n  </div>\n\n\n\n  <div class=\"col-9\">\n    <div class=\"row\">\n      <div class=\"col-md-5\">\n        <h6>Enter Promotional Code or Gift Card</h6>\n      </div>\n      <div class=\"col-md-5\">\n        <div class=\"input-group\">\n          <input type=\"text\" value='' [(ngModel)]='codeValue' class=\"form-control\">\n          <span class=\"input-group-btn\">\n            <button class=\"btn btn-outline-secondary\" (click)='applyCoupan(codeValue)' type=\"button\">APPLY</button>\n          </span>\n        </div>\n      </div>\n    </div>\n    <hr style=\"height:10px; border:none; color:rgb(220, 223, 230); background-color:rgb(220, 223, 230);\">\n    <div class=\"row\">\n        <div class=\"col-md-5\">\n            <h6>SUBTOTAL</h6>\n          </div>\n          <div class=\"col-md-5 price\">\n              $ {{subTotal}}\n          </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-md-5\">\n            <h6>PROMOTION CODE <b>{{codeValue}}</b> APPLIED</h6>\n          </div>\n          <div class=\"col-md-5 price\">\n               {{appliedCode}}\n          </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-md-5\">\n            <h6>ESTIMATED SHIPPING CHARGES</h6>\n          </div>\n          <div class=\"col-md-5 price\">\n              FREE\n          </div>\n    </div>\n    <hr style=\"height:10px; border:none; color:rgb(220, 223, 230); background-color:rgb(220, 223, 230);\">\n    <div class=\"row\">\n        <div class=\"col-md-5\">\n            <h5>ESTIMATED TOTAL</h5>\n          </div>\n          <div class=\"col-md-5 price\">\n           <b> $ {{estimatedValue}}</b>\n          </div>\n    </div>\n    <hr style=\"height:10px; border:none; color:rgb(220, 223, 230); background-color:rgb(220, 223, 230);\">\n    <div class=\"row\">\n        <div class=\"col-md-5\">\n          </div>\n          <div class=\"col-md-5 price\">\n            <a href='' style=\"margin: 10px;\"><u>Continue Shopping</u></a>\n            <button class=\"btn btn-primary float-right\" type=\"submit\">CHECKOUT</button>\n          </div>\n    </div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/shopping-bag/view-bag/view-bag.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/shopping-bag/view-bag/view-bag.component.ts ***!
  \*************************************************************/
/*! exports provided: ViewBagComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewBagComponent", function() { return ViewBagComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shopping_bag_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shopping-bag.service */ "./src/app/shopping-bag/shopping-bag.service.ts");



var ViewBagComponent = /** @class */ (function () {
    function ViewBagComponent(BagService) {
        this.BagService = BagService;
        this.OfferList = [];
        this.finalSubtotal = 0;
        this.subTotal = 0;
        this.codeValue = '';
        this.prevCodeValue = 'some coupon';
        this.appliedCode = 0;
        this.estimatedValue = 0;
        this.count = 0;
        this.test2 = 0;
        this.ProductList = [];
        this.model = {};
        this.inputField = [];
    }
    ViewBagComponent.prototype.ngOnInit = function () {
        this.getOffers();
        this.getProducts();
    };
    ViewBagComponent.prototype.getOffers = function () {
        var _this = this;
        this.BagService.getOffers().subscribe(function (data) {
            _this.OfferList = data;
        });
    };
    ViewBagComponent.prototype.getProducts = function () {
        var _this = this;
        this.BagService.getProducts().subscribe(function (data) {
            console.log('ProductList', data);
            _this.ProductList = data;
            _this.ProductList.forEach(function (element, index) {
                element.image = "/assets/" + element.image_name;
                element.type = 'number',
                    element.name = 'field' + index,
                    element.value = 1;
            });
            _this.calculatePrice(_this.ProductList);
        });
    };
    ViewBagComponent.prototype.calculatePrice = function (ProductList) {
        var _this = this;
        this.codeValue = '';
        this.prevCodeValue = 'some coupon';
        this.appliedCode = 0;
        this.subTotal = 0;
        var list = ProductList;
        list.forEach(function (element) {
            if (element.gender == 'Female') {
                _this.subTotal += (element.value * element.product_price) - Math.round((element.value * element.product_price * (element.discount_percent + 5)) / 100);
            }
            else {
                _this.subTotal += (element.value * element.product_price) - Math.round((element.value * element.product_price * element.discount_percent) / 100);
            }
        });
        this.finalSubtotal = this.subTotal;
        this.estimatedValue = this.subTotal;
    };
    ViewBagComponent.prototype.applyCoupan = function (codeValue) {
        var _this = this;
        this.count = this.count + 1;
        this.subTotal = this.finalSubtotal;
        if (this.prevCodeValue != codeValue) {
            var test_1 = '';
            test_1 = codeValue;
            var obj = this.OfferList.find(function (o) { return o.offer_code === test_1.trim(); });
            if (obj) {
                this.prevCodeValue = test_1;
                this.appliedCode = Math.round((obj.discount_percent * this.subTotal) / 100);
                this.codeValue = test_1;
                this.subTotal = this.subTotal - this.appliedCode;
                this.test2 = this.subTotal;
            }
            else {
                alert('Please Apply a valid Coupan for more Discount');
                this.subTotal = 0;
                this.ProductList.forEach(function (element) {
                    _this.subTotal += element.value * element.product_price;
                });
                this.appliedCode = 0;
                this.codeValue = '';
            }
            this.estimatedValue = this.subTotal;
        }
        else {
            this.subTotal = this.test2;
            alert('This coupon was already applied...Try other one.');
        }
    };
    ViewBagComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-view-bag',
            template: __webpack_require__(/*! ./view-bag.component.html */ "./src/app/shopping-bag/view-bag/view-bag.component.html"),
            styles: [__webpack_require__(/*! ./view-bag.component.css */ "./src/app/shopping-bag/view-bag/view-bag.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shopping_bag_service__WEBPACK_IMPORTED_MODULE_2__["ShoppingBagService"]])
    ], ViewBagComponent);
    return ViewBagComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Jarvis\Desktop\Assignment Semusi\semusi-assignment-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map